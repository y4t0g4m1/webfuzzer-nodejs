export const requestStatus = {
    submitted: 'Submitted',
    queued: 'Queued',
    processing: 'Processing',
    completed: 'Completed',
    error: 'Error'
};

export const savingLogStatus = {
    db: 'db',
    csv: 'csv'
}
import React, { Component } from 'react'

export class AwaitingComponent extends Component {
    render() {
        return (
            <div>
                <div className="awaiting-component">
                    <div className="loadingio-spinner-dual-ball-bhp6v9njld6">
                    <div className="ldio-ubx67jzuou8">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    </div>
                    <div className="awaiting-text">
                        Awaiting...
                    </div>
                </div>
                
            </div>
        )
    }
}

export default AwaitingComponent
